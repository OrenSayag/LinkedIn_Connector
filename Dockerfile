FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install gnupg wget -y && dpkg --add-architecture i386 && wget -nc https://dl.winehq.org/wine-builds/winehq.key && apt-key add winehq.key && apt-get install software-properties-common -y && apt-get update && apt-get -y install --no-install-recommends xorg openbox libnss3 libasound2 libatk-adaptor libgtk-3-0 && apt-get update && apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ cosmic main' && apt-get install --install-recommends winehq-stable -y && apt-get install nodejs npm -y
COPY . /linkedin-connector
WORKDIR ./linkedin-connector
RUN npm i -f && npm i -D electron@latest && npm run build && cp -R ./electron ./build && npm run package && rm -rf node_modules Dockerfile build electron package.json src README.md package-lock.json public yarn.lock
